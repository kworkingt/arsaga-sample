import React from 'react';
import ReactModal from 'react-modal';

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};
ReactModal.setAppElement('#map')

class Modal extends React.Component {
  
  render () {
    const records = this.props.records.map((record, i) =>
      <li key={i} className="list-group-item">
        <a href={record.page_url} rel="noreferrer" target="_blank">{record.place} {record.start}~{record.end}</a>
      </li>
    );

    return (
      <div>
        <ReactModal
          className="col-md-8 col-lg-10"
           isOpen={this.props.showModal}
           contentLabel="Minimal Modal Example"
           onRequestClose={this.props.onModalClose}
           style={customStyles}
        >
            <div className="" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title">{this.props.mountain.name}</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.onModalClose}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                <ul className="list-group record-list">
                  {records}
                </ul> 
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.props.onModalClose}>Close</button>
                </div>
              </div>
            </div>
        </ReactModal>
      </div>
    );
  }
}
export default Modal;
