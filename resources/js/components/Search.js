import React, { Component } from 'react';

export default class search extends Component {
  render() {
    // modalがopen時はformを後ろに隠す
    const inputStyle = this.props.showModal ? {zIndex: 0} : {zIndex: 1}
    return (
      <div className="row justify-content-center search-wrapper">
        <div className="search col-9 col-md-7 col-lg-5" style={inputStyle}>
        <input className="form-control" onChange={this.props.onChange} placeholder="search" />
        </div>
      </div>
    );
  }
}