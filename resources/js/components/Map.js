import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import GoogleMapReact from 'google-map-react';
import Modal from './Modal';
import Marker from './Marker';
import Search from './Search';
import nearby from './nearby.json'
import records from './records.json'

export default class Map extends Component {
  constructor(props) {
    super(props);
    this._onBoundsChange = this._onBoundsChange.bind(this);
    this._onModalClose = this._onModalClose.bind(this);
    this._onChildClick = this._onChildClick.bind(this);
    this._onInputChange = this._onInputChange.bind(this);


    this.state = {
      mountains: [], //山データ
      records: [], //山行記録データ
      center: {
        lat: 36.406722,
        lng: 137.712778
      },
      clickedMountain: {},
      showModal: false,
      query: ''
    };
  }

  static defaultProps = {
    zoom: 11,
  };

  componentDidMount() {
    let lat = this.state.center.lat;
    let lng = this.state.center.lng;
    this.fetchNearbyMt(lat, lng)
  }

  /**
   * マップが移動後、中央付近の山データを取得する
   */
  _onBoundsChange(center, zoom, bounds, marginBounds) {
    let lat = center[0];
    let lng = center[1];
    this.fetchNearbyMt(lat, lng);
  }

  _onModalClose = (e) => {
    this.setState({showModal: false})
  }


  /**
   * マップが移動後、中央付近の山データを取得する
   */
  _onChildClick = (key, childProps) => {
    this.setState({ 
      clickedMountain: childProps.mountain,
      showModal: true
    })
    this.fetchRecord()
  }

  _onInputChange = (e) => {
    this.setState({query: e.target.value});
  }

  /**
   * lat, lng付近の山データを取得する
   */
  fetchNearbyMt = (lat, lng) => {
    this.setState({ 
      mountains: nearby.poilist
    })
    // return fetch('api/yamareco/nearbyMt?page=1&range=30&type_id=1&lat=' + lat + '&lon=' + lng)
    // .then((response) => response.json())
    // .then((responseJson) => {
    //   if (!responseJson.err) {
    //     this.setState({ 
    //       mountains: this.state.mountains.concat(responseJson.poilist)
    //     })
    //   }
    // })
    // .catch((error) => {
    //   console.error(error);
    // });
  }

  /**
   * 山行記録データを取得する
   */
  fetchRecord = () => {

    this.setState({ 
      records: records.reclist
    })

    // return fetch('api/yamareco/record?page=1')
    // .then((response) => response.json())
    // .then((responseJson) => {
    //   if (!responseJson.err) {
    //     this.setState({ 
    //       records: responseJson.reclist
    //     })
    //   }
    // })
    // .catch((error) => {
    //   console.error(error);
    // });
  }

  render() {
    let mountains = this.state.mountains;

    // 検索フォームに文字があれば、その文字でフィルタリング
    if (this.state.query !== '') {
      mountains = mountains.filter(mountain => mountain.name.includes(this.state.query));
    }

    const markers = mountains.map((mountain, i) =>
      <Marker
        style={{zIndex:{i},width: "50px", height: "50px"}}
        key={i}
        lat={mountain.lat}
        lng={mountain.lon}
        text={mountain.name}
        mountain={mountain}
      />
    );
    console.log(process.env.GOOGLE_MAP_API_KEY)

    return (
      <div className="map-react">
        <Search onChange={this._onInputChange} showModal={this.state.showModal} />
        <GoogleMapReact
          // bootstrapURLKeys={{ key: process.env.GOOGLE_MAP_API_KEY }}
          bootstrapURLKeys={{ key: 'AIzaSyDWYcktL5tQv7odQ1Og3EU-CqY1FZLehXQ' }}
          defaultCenter={this.state.center}
          defaultZoom={this.props.zoom}
          onBoundsChange={this._onBoundsChange}
          onChildClick={this._onChildClick}
          >
          {markers}
        </GoogleMapReact>
        <Modal
          onModalClose={this._onModalClose}
          showModal={this.state.showModal}
          records={this.state.records}
          mountain={this.state.clickedMountain}
        />
      </div>
    );
  }
}

if (document.getElementById('map')) {
  ReactDOM.render(<Map />, document.getElementById('map'));
}